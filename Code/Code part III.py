import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
matplotlib.style.use('ggplot')

part3 = os.path.abspath("../Data/part3.xlsx")
df3 = pd.read_excel(part3)
print(df3)


#determine the distribution of clients
fig, ax = plt.subplots()
ax.hist(df3['CLIENT_TYPE'],bins=20, edgecolor="white")
plt.show()

counts = pd.Series(df3['CLIENT_TYPE']).value_counts()
print(counts.get('Business')) #4170
print(counts.get('Healthy')) #7913
print(counts.get('Retired')) #5482
print(counts.get('Onetime')) #18935

len(df3['CLIENT_TYPE']) #36500

#another way to vizualize the distribution

labels = 'Business', 'Healthy', 'Retired', 'Onetime'
sizes = [counts.get('Business')/len(df3['CLIENT_TYPE']),
         counts.get('Healthy')/len(df3['CLIENT_TYPE']),
         counts.get('Retired')/len(df3['CLIENT_TYPE']),
         counts.get('Onetime')/len(df3['CLIENT_TYPE'])]
explode = (0.1, 0.1, 0.1, 0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()

#likelihood of these clients to get a certain course
#we create new columns that counts if the client had a course or not
df3['FIRST_COURSE_NUMBER'] = df3['FIRST_COURSE'].apply(
        lambda x: 0 if x==0 else 1)
df3['SECOND_COURSE_NUMBER'] = df3['SECOND_COURSE'].apply(
        lambda x: 0 if x==0 else 1)
df3['THIRD_COURSE_NUMBER'] = df3['THIRD_COURSE'].apply(
        lambda x: 0 if x==0 else 1)


grouped = df3.groupby('CLIENT_TYPE').agg(  {'FIRST_COURSE_NUMBER': sum,
                                      'SECOND_COURSE_NUMBER':sum,
                                      'THIRD_COURSE_NUMBER':sum}).reset_index()
#we plot the charts
#for the first course
labels = 'Business', 'Healthy', 'Onetime','Retired'
sizes = np.array(grouped['FIRST_COURSE_NUMBER']/grouped['FIRST_COURSE_NUMBER'].sum())
explode = (0.1, 0.1, 0.1, 0.1)

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')

plt.show()

#for the second course
labels = 'Business', 'Healthy', 'Onetime','Retired'
sizes = np.array(grouped['SECOND_COURSE_NUMBER']/grouped['SECOND_COURSE_NUMBER'].sum())
explode = (0.1, 0.1, 0.1, 0.1)

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')

plt.show()

#for the third course
labels = 'Business', 'Healthy', 'Onetime','Retired'
sizes = np.array(grouped['THIRD_COURSE_NUMBER']/grouped['THIRD_COURSE_NUMBER'].sum())
explode = (0.1, 0.1, 0.1, 0.1)

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')

plt.show()

#probability of a certain type of customer ordering a certain dish
#we create a column with the name of the dishes
FOODNAME_FIRST_COURSE = []
for row in df3['FIRST_COURSE']:
    if row < 3:
        FOODNAME_FIRST_COURSE.append('Nothing')
    elif row < 15:
        FOODNAME_FIRST_COURSE.append('Soup')
    elif row < 20:
        FOODNAME_FIRST_COURSE.append('Tomato-Mozarella')
    else:
        FOODNAME_FIRST_COURSE.append('Oysters')
df3['FOODNAME_FIRST_COURSE'] = FOODNAME_FIRST_COURSE

FOODNAME_SECOND_COURSE = []
for row in df3['SECOND_COURSE']:
    if row < 9:
        FOODNAME_SECOND_COURSE.append('Nothing')
    elif row < 20:
        FOODNAME_SECOND_COURSE.append('Salad')
    elif row < 25:
        FOODNAME_SECOND_COURSE.append('Spaghetti')
    elif row < 40:
        FOODNAME_SECOND_COURSE.append('Steak')
    else:
        FOODNAME_SECOND_COURSE.append('Lobster')
df3['FOODNAME_SECOND_COURSE'] = FOODNAME_SECOND_COURSE

FOODNAME_THIRD_COURSE = []
for row in df3['SECOND_COURSE']:
    if row < 10:
        FOODNAME_THIRD_COURSE.append('Nothing')
    elif row < 15:
        FOODNAME_THIRD_COURSE.append('Pie')
    else:
        FOODNAME_THIRD_COURSE.append('Ice Cream')
df3['FOODNAME_THIRD_COURSE'] = FOODNAME_THIRD_COURSE

#we figure out the likelihood of dishes per customer
df3_likelihood = df3[['CLIENT_TYPE','FOODNAME_FIRST_COURSE','FOODNAME_SECOND_COURSE','FOODNAME_THIRD_COURSE']]

melted_df = pd.melt(df3_likelihood, id_vars='CLIENT_TYPE', value_vars=['FOODNAME_FIRST_COURSE','FOODNAME_SECOND_COURSE','FOODNAME_THIRD_COURSE'])
melted_df['count'] = 1
melted_df_grouped = melted_df.groupby(['CLIENT_TYPE', 'value']).agg({'count':sum}).reset_index()

info_groups =  {}

for g in melted_df_grouped['CLIENT_TYPE'].unique():
    info_groups[g] = melted_df_grouped[melted_df_grouped['CLIENT_TYPE'] == g]
    info_groups[g]['percentage'] = info_groups[g]['count']/info_groups[g]['count'].sum()


for key in info_groups.keys():

    labels = info_groups[key]['value']
    sizes = info_groups[key]['percentage']
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')
    plt.title(key)
    plt.show()


#we figure out the distribution of dishes per customer per course
df3_likelihood['count'] = 1

df_business = df3_likelihood[df3_likelihood['CLIENT_TYPE'] == 'Business']
df_healthy = df3_likelihood[df3_likelihood['CLIENT_TYPE'] == 'Healthy']
df_onetime = df3_likelihood[df3_likelihood['CLIENT_TYPE'] == 'Onetime']
df_retired = df3_likelihood[df3_likelihood['CLIENT_TYPE'] == 'Retired']

df_business_count_1st = df_business.groupby('FOODNAME_FIRST_COURSE').agg({'count':sum}).reset_index()

df_business_count_2nd = df_business.groupby('FOODNAME_SECOND_COURSE').agg({'count':sum}).reset_index()

df_business_count_3rd = df_business.groupby('FOODNAME_THIRD_COURSE').agg({'count':sum}).reset_index()


df_onetime_count_1st = df_onetime.groupby('FOODNAME_FIRST_COURSE').agg({'count':sum}).reset_index()

df_onetime_count_2nd = df_onetime.groupby('FOODNAME_SECOND_COURSE').agg({'count':sum}).reset_index()

df_onetime_count_3rd = df_onetime.groupby('FOODNAME_THIRD_COURSE').agg({'count':sum}).reset_index()


df_retired_count_1st = df_retired.groupby('FOODNAME_FIRST_COURSE').agg({'count':sum}).reset_index()

df_retired_count_2nd = df_retired.groupby('FOODNAME_SECOND_COURSE').agg({'count':sum}).reset_index()

df_retired_count_3rd = df_retired.groupby('FOODNAME_THIRD_COURSE').agg({'count':sum}).reset_index()


df_healthy_count_1st = df_healthy.groupby('FOODNAME_FIRST_COURSE').agg({'count':sum}).reset_index()

df_healthy_count_2nd = df_healthy.groupby('FOODNAME_SECOND_COURSE').agg({'count':sum}).reset_index()

df_healthy_count_3rd = df_healthy.groupby('FOODNAME_THIRD_COURSE').agg({'count':sum}).reset_index()

info_charts = {
    'business_1st_course': df_business_count_1st,
    'business_2nd_course': df_business_count_2nd,
    'business_3rd_course': df_business_count_3rd,
    'retired_1st_course': df_retired_count_1st,
    'retired_2nd_course': df_retired_count_2nd,
    'retired_3rd_course': df_retired_count_3rd,
    'healthy_1st_course': df_healthy_count_1st,
    'healthy_2nd_course': df_healthy_count_2nd,
    'healthy_3rd_course': df_healthy_count_3rd,
    'onetime_1st_course': df_onetime_count_1st,
    'onetime_2nd_course': df_onetime_count_2nd,
    'onetime_3rd_course': df_onetime_count_3rd,

}


for key in info_charts.keys():
    info_charts[key]['percentage'] = info_charts[key]['count']/info_charts[key]['count'].sum()
    info_charts[key].columns = ['dish', 'count', 'percentage']
    labels = info_charts[key]['dish']
    sizes =  info_charts[key]['percentage']
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')
    plt.title(key)
    plt.show()

#determine the distribution of the costs of drinks per course
DRINKS_FIRST_COURSE = []
for row in df3['FIRST_COURSE']:
    if row < 3:
        DRINKS_FIRST_COURSE.append(0)
    elif row < 15:
        DRINKS_FIRST_COURSE.append(row - 3)
    elif row < 20:
        DRINKS_FIRST_COURSE.append(row - 15)
    else:
        DRINKS_FIRST_COURSE.append(row - 20)
df3['DRINKS_FIRST_COURSE'] = DRINKS_FIRST_COURSE

DRINKS_SECOND_COURSE = []
for row in df3['SECOND_COURSE']:
    if row < 9:
        DRINKS_SECOND_COURSE.append(0)
    elif row < 20:
        DRINKS_SECOND_COURSE.append(row - 9)
    elif row < 25:
        DRINKS_SECOND_COURSE.append(row - 20)
    elif row < 40:
        DRINKS_SECOND_COURSE.append(row - 25)
    else:
        DRINKS_SECOND_COURSE.append(row - 40)
df3['DRINKS_SECOND_COURSE'] = DRINKS_SECOND_COURSE

DRINKS_THIRD_COURSE = []
for row in df3['THIRD_COURSE']:
    if row < 10:
        DRINKS_THIRD_COURSE.append(0)
    elif row < 15:
        DRINKS_THIRD_COURSE.append(row - 10)
    else:
        DRINKS_THIRD_COURSE.append(row - 15)
df3['DRINKS_THIRD_COURSE'] = DRINKS_THIRD_COURSE
fig, ax = plt.subplots()
ax.hist(df3['DRINKS_FIRST_COURSE'],bins=20, edgecolor="white")
plt.show()

fig, ax = plt.subplots()
ax.hist(df3['DRINKS_SECOND_COURSE'],bins=20, edgecolor="white")
plt.show()

fig, ax = plt.subplots()
ax.hist(df3['DRINKS_THIRD_COURSE'],bins=20, edgecolor="white")
plt.show()