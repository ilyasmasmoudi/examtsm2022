import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn import datasets

part2 = "C:\\Users\\imasm\\PycharmProjects\\examtsm2022\\Data\\part1.csv"
df2 = pd.read_csv(part2, sep=",")
path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))

X = df2[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]] #subsetting the initial dataset

kmeans = KMeans(n_clusters=4)
y = kmeans.fit_predict(X) #using the K-means technique

df2["clusters"] = y #creating the column clusters

if 'part2b.xlsx' not in os.listdir():
    df2.to_excel(os.path.join(path,"Data","part2b.xlsx"))