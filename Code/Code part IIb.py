import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn import datasets

#we import here the dataset we have created in partIIa
part2b = os.path.abspath("../Data/part2b.xlsx")
df2b = pd.read_excel(part2b)
path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
print(df2b)

#
df2b["total_course"] = df2b[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]].sum(axis=1) #computing the sum of the courses for each cluster

info_ = {}
for i in df2b["clusters"].unique():
    info_[i] = np.mean(df2b[df2b["clusters"] == i]["total_course"]) #computing the mean of the cost of the food for each groups

info_ #we have the mean of the expenses for each group
info_

#we assume then that
#group 0 is healthy clients
#group 1 is retired clients
#group 2 is business clients
#group 3 is onetime clients
#group
#we create a new column in the dataset
CLIENT_TYPE = []
for row in df2b['clusters']:
    if row == 0:
        CLIENT_TYPE.append('Healthy')
    elif row == 1:
        CLIENT_TYPE.append('Retired')
    elif row == 2:
        CLIENT_TYPE.append('Business')
    else:
        CLIENT_TYPE.append('Onetime')
df2b['CLIENT_TYPE'] = CLIENT_TYPE
print(df2b)

#PART 3
#determine the distribution of clients
fig, ax = plt.subplots()
ax.hist(df2b['CLIENT_TYPE'],bins=20, edgecolor="white")
plt.show()

counts = pd.Series(df2b['CLIENT_TYPE']).value_counts()
print(counts.get('Business'))
print(counts.get('Healthy'))
print(counts.get('Retired'))
print(counts.get('Onetime'))

len(df2b['CLIENT_TYPE'])
print(df2b)

if 'part3.xlsx' not in os.listdir():
    df2b.to_excel(os.path.join(path,"Data","part3.xlsx"))