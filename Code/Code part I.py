import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
matplotlib.style.use('ggplot')

part1 = os.path.abspath("../Data/part1.csv")
df1 = pd.read_csv(part1, sep=",")
print(df1)

#making a plot of the cost of each courses
fig, ax = plt.subplots()
ax.hist(df1['FIRST_COURSE'],bins=20, edgecolor="white")
plt.show()
fig, ax = plt.subplots()
ax.hist(df1['SECOND_COURSE'],bins=20, edgecolor="white")
plt.show()
fig, ax = plt.subplots()
ax.hist(df1['THIRD_COURSE'],bins=20, edgecolor="white")
plt.show()

#making a barplot of the cost per course
xAxis = ['FIRST COURSE','SECOND COURSE','THIRD COURSE']
yAxis = [len(df1[df1['FIRST_COURSE']!=0]),len(df1[df1['SECOND_COURSE']!=0]),len(df1[df1['THIRD_COURSE']!=0])]
plt.bar(xAxis,yAxis)
plt.show()

#creating the 6 additional columns

#the column with the cost of the food for the first course
FOOD_FIRST_COURSE = []
for row in df1['FIRST_COURSE']:
    if row < 3:
        FOOD_FIRST_COURSE.append(0)
    elif row < 15:
        FOOD_FIRST_COURSE.append(3)
    elif row < 20:
        FOOD_FIRST_COURSE.append(15)
    else:
        FOOD_FIRST_COURSE.append(20)
df1['FOOD_FIRST_COURSE'] = FOOD_FIRST_COURSE

#the column with the cost of the drinks for the first course
DRINKS_FIRST_COURSE = []
for row in df1['FIRST_COURSE']:
    if row < 3:
        DRINKS_FIRST_COURSE.append(0)
    elif row < 15:
        DRINKS_FIRST_COURSE.append(row - 3)
    elif row < 20:
        DRINKS_FIRST_COURSE.append(row - 15)
    else:
        DRINKS_FIRST_COURSE.append(row - 20)
df1['DRINKS_FIRST_COURSE'] = DRINKS_FIRST_COURSE

#food cost for the second course
FOOD_SECOND_COURSE = []
for row in df1['SECOND_COURSE']:
    if row < 9:
        FOOD_SECOND_COURSE.append(0)
    elif row < 20:
        FOOD_SECOND_COURSE.append(9)
    elif row < 25:
        FOOD_SECOND_COURSE.append(20)
    elif row < 40:
        FOOD_SECOND_COURSE.append(25)
    else:
        FOOD_SECOND_COURSE.append(40)
df1['FOOD_SECOND_COURSE'] = FOOD_SECOND_COURSE

#drink cost for the second course
DRINKS_SECOND_COURSE = []
for row in df1['SECOND_COURSE']:
    if row < 9:
        DRINKS_SECOND_COURSE.append(0)
    elif row < 20:
        DRINKS_SECOND_COURSE.append(row - 9)
    elif row < 25:
        DRINKS_SECOND_COURSE.append(row - 20)
    elif row < 40:
        DRINKS_SECOND_COURSE.append(row - 25)
    else:
        DRINKS_SECOND_COURSE.append(row - 40)
df1['DRINKS_SECOND_COURSE'] = DRINKS_SECOND_COURSE

#food cost for the third course
FOOD_THIRD_COURSE = []
for row in df1['THIRD_COURSE']:
    if row < 10:
        FOOD_THIRD_COURSE.append(0)
    elif row < 15:
        FOOD_THIRD_COURSE.append(10)
    else:
        FOOD_THIRD_COURSE.append(15)
df1['FOOD_THIRD_COURSE'] = FOOD_THIRD_COURSE

#drink cost for the third course
DRINKS_THIRD_COURSE = []
for row in df1['THIRD_COURSE']:
    if row < 10:
        DRINKS_THIRD_COURSE.append(0)
    elif row < 15:
        DRINKS_THIRD_COURSE.append(row - 10)
    else:
        DRINKS_THIRD_COURSE.append(row - 15)
df1['DRINKS_THIRD_COURSE'] = DRINKS_THIRD_COURSE

print(df1)
