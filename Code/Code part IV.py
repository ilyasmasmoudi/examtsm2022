import pandas as pd
import numpy as np
import os
import random
import string

df = pd.DataFrame()
path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))

TIME=['Lunch','Dinner']
CUSTOMERTYPE = ['Business', 'Retired', 'Onetime','Healthy']
COURSE1 = ['Soup', 'Tomato-Mozarella', 'Oysters']
COURSE2 = ['Salad', 'Spaghetti', 'Steak','Lobster']
COURSE3 = ['Ice cream', 'Pie']
DRINKS1 = random.choice(range(1,12))
DRINKS2 = random.choice(range(1,15))
DRINKS3 = random.choice(range(1,5))
TOTAL1 = ['3','15','20']
TOTAL2 = ['9','20','25','40']
TOTAL3 = ['15','10']

def customerid():
    return "ID" + str(np.random.randint(100000, 1000000))

list_customerid = []

for i in range(36500):
    list_customerid.append(customerid())

df["CUSTOMERID"] = list_customerid
df["TIME"] = np.random.choice(TIME, size=36500)
df["CUSTOMERTYPE"] = np.random.choice(CUSTOMERTYPE, size=36500)
df["COURSE1"] = np.random.choice(COURSE1, size=36500)
df["COURSE2"] = np.random.choice(COURSE2, size=36500)
df["COURSE3"] = np.random.choice(COURSE3, size=36500)
df["DRINKS1"] = np.random.choice(DRINKS1, size=36500)
df["DRINKS2"] = np.random.choice(DRINKS2, size=36500)
df["DRINKS3"] = np.random.choice(DRINKS3, size=36500)
df["TOTAL1_1"] = np.random.choice(TOTAL1, size=36500)
df["TOTAL2_1"] = np.random.choice(TOTAL2, size=36500)
df["TOTAL3_1"] = np.random.choice(TOTAL3, size=36500)
df["TOTAL1"] = df["DRINKS1"].apply(float) + df["TOTAL1_1"].apply(float)
df["TOTAL2"] = df["DRINKS2"].apply(float) + df["TOTAL2_1"].apply(float)
df["TOTAL3"] = df["DRINKS3"].apply(float) + df["TOTAL3_1"].apply(float)
df.drop(["TOTAL1_1","TOTAL2_1","TOTAL3_1"],axis=1,inplace=True)

print(df)
if 'part4.xlsx' not in os.listdir():
    df.to_excel(os.path.join(path,"Data","part4.xlsx"))